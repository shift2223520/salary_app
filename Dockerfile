FROM ubuntu:24.04
RUN apt update
RUN apt install pipx -y
ENV PATH=$PATH:/root/.local/bin
RUN pipx install poetry
WORKDIR /usr/app
COPY pyproject.toml pyproject.toml
COPY poetry.lock poetry.lock
RUN poetry install --no-root --no-cache
COPY . .
CMD ["poetry", "run", "uvicorn", "--host", "0.0.0.0", "main:app"]