import datetime
import os
from pathlib import Path

_db_dir = Path(os.environ.get("SALARY_APP_DB_DIRECTORY", "db_storage"))
_db_dir.mkdir(parents=True, exist_ok=True)
_db_path = _db_dir.joinpath("salary_app.db")

db_url = f"sqlite:///{_db_path}"

_session_token_expiration_period_seconds = os.environ.get("SALARY_APP_TOKEN_EXPIRATION_IN_SECONDS", 3600)

session_token_expiration_period = datetime.timedelta(seconds=_session_token_expiration_period_seconds)
