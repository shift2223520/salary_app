from sqlalchemy import Engine, create_engine
from sqlalchemy_utils.functions import database_exists

from salary_app.entities import User

from .tables import Base
from .storage import SqlUserStorage
from .services import SqlPermissionsHandler, SqlAuthorizationService


def connect_and_init_if_needed(url: str) -> Engine:
    uninitialized = not database_exists(url)
    engine = create_engine(url)

    if uninitialized:
        init_db(engine)

    return engine


def init_db(engine):
    Base.metadata.create_all(engine)

    user_storage = SqlUserStorage(engine)
    permissions_handler = SqlPermissionsHandler(engine)
    auth_service = SqlAuthorizationService(engine)

    add_admin(auth_service, permissions_handler, user_storage)


def add_admin(auth_service, permissions_handler, user_storage):
    user_storage.create(User(user_id="0", login="admin"))
    auth_service.create_user(authorized_user="0", password="admin")
    permissions_handler.make_admin("0")
