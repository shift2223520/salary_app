from sqlalchemy import Engine, select, update
from sqlalchemy.orm import Session
from argon2 import PasswordHasher
from argon2.exceptions import VerifyMismatchError

from salary_app.entities import UserId
from salary_app.salary_app import AuthorizationService, PermissionsHandler

from .tables import SqlUser, SqlAuth


class SqlPermissionsHandler(PermissionsHandler):
    def __init__(self, engine: Engine):
        self._engine = engine

    def is_admin(self, user_id: UserId) -> bool:
        with Session(self._engine) as session:
            stmt = select(SqlUser.is_admin).where(SqlUser.user_id.is_(user_id))
            return next(session.scalars(stmt), False)

    def make_admin(self, user_id: UserId):
        with Session(self._engine) as session:
            stmt = update(SqlUser).where(SqlUser.user_id.is_(user_id)).values(is_admin=True)
            session.execute(stmt)
            session.commit()


class SqlAuthorizationService(AuthorizationService):
    def __init__(self, engine: Engine):
        self._engine = engine

    def verify_user(self, user_id: UserId, password: str) -> bool:
        with Session(self._engine) as session:
            stmt = select(SqlAuth.hash).where(SqlAuth.user_id.is_(user_id))
            hash_value = next(session.scalars(stmt), None)
            if hash_value is None:
                return False

            password_hasher = PasswordHasher()
            try:
                password_hasher.verify(hash=hash_value, password=password)
                return True
            except VerifyMismatchError:
                return False

    def create_user(self, user_id: UserId, password: str):
        with Session(self._engine) as session:
            password_hasher = PasswordHasher()
            hash_value = password_hasher.hash(password)
            session.add(SqlAuth(user_id=user_id, hash=hash_value))
            session.commit()
