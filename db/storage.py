import datetime
from datetime import date
from typing import Optional

from sqlalchemy import Engine, select
from sqlalchemy.orm import Session

from salary_app.entities import UserId, User, EmployeeId, Employee
from salary_app.storage import UserStorage, EmployeeStorage, PromotionStorage, Storage

from .tables import SqlUser, SqlEmployee, SqlPlannedPromotion


class SqlUserStorage(UserStorage):
    def __init__(self, engine: Engine):
        self._engine = engine

    def fetch(self, user_id: UserId) -> Optional[User]:
        with Session(self._engine) as session:
            stmt = select(SqlUser).where(SqlUser.user_id.is_(user_id))
            for user in session.scalars(stmt):
                return User(user_id=user.user_id, login=user.login)
        return None

    def create(self, user: User):
        with Session(self._engine) as session:
            session.add(
                SqlUser(user_id=user.user_id, login=user.login)
            )
            session.commit()

    def find_by_login(self, login: str) -> Optional[UserId]:
        with Session(self._engine) as session:
            stmt = select(SqlUser.user_id).where(SqlUser.login.is_(login))
            return next(session.scalars(stmt), None)


class SqlEmployeeStorage(EmployeeStorage):
    def __init__(self, engine: Engine):
        self._engine = engine

    def fetch(self, employee_id: EmployeeId) -> Optional[Employee]:
        with Session(self._engine) as session:
            stmt = select(SqlEmployee).where(SqlEmployee.employee_id.is_(employee_id))
            for sql_employee in session.scalars(stmt):
                return Employee(
                    employee_id=sql_employee.employee_id,
                    name=sql_employee.name,
                    salary=sql_employee.salary,
                )
        return None

    def create(self, user_id: UserId, employee: Employee):
        with Session(self._engine) as session:
            session.add(
                SqlEmployee(
                    employee_id=employee.employee_id,
                    user_id=user_id,
                    name=employee.name,
                    salary=employee.salary,
                )
            )
            session.commit()

    def find_by_user_id(self, user_id: UserId) -> Optional[EmployeeId]:
        with Session(self._engine) as session:
            stmt = select(SqlEmployee.employee_id).where(SqlEmployee.user_id.is_(user_id))
            return next(session.scalars(stmt), None)


class SqlPromotionStorage(PromotionStorage):
    def __init__(self, engine: Engine):
        self._engine = engine

    def fetch_upcoming(self, employee_id: EmployeeId) -> Optional[date]:
        with Session(self._engine) as session:
            stmt = select(SqlPlannedPromotion.date) \
                .where(SqlPlannedPromotion.employee_id.is_(employee_id)) \
                .limit(1) \
                .order_by(SqlPlannedPromotion.date)

            return next(session.scalars(stmt), None)

    def create(self, employee_id: EmployeeId, promotion_date: datetime.date):
        with Session(self._engine) as session:
            session.add(SqlPlannedPromotion(employee_id=employee_id, date=promotion_date))
            session.commit()


class SqlStorage(Storage):
    def __init__(self, engin: Engine):
        self._user = SqlUserStorage(engin)
        self._employee = SqlEmployeeStorage(engin)
        self._promotion = SqlPromotionStorage(engin)

    @property
    def user(self) -> UserStorage:
        return self._user

    @property
    def employee(self) -> EmployeeStorage:
        return self._employee

    @property
    def promotion(self) -> PromotionStorage:
        return self._promotion
