import datetime
from decimal import Decimal

from sqlalchemy import String, ForeignKey, Numeric
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column


class Base(DeclarativeBase):
    pass


class SqlUser(Base):
    __tablename__ = "users"

    user_id: Mapped[str] = mapped_column(String(50), primary_key=True)
    login: Mapped[str] = mapped_column(unique=True)
    is_admin: Mapped[bool] = mapped_column(default=False)


class SqlEmployee(Base):
    __tablename__ = "employees"

    employee_id: Mapped[str] = mapped_column(String(50), primary_key=True)
    user_id: Mapped[str] = mapped_column(ForeignKey("users.user_id"))
    name: Mapped[str]
    salary: Mapped[Decimal] = mapped_column(Numeric(precision=15, scale=2))


class SqlPlannedPromotion(Base):
    __tablename__ = "promotions"

    employee_id: Mapped[str] = mapped_column(ForeignKey("employees.employee_id"), primary_key=True)
    date: Mapped[datetime.date] = mapped_column(primary_key=True)


class SqlAuth(Base):
    __tablename__ = "auth"

    user_id: Mapped[str] = mapped_column(ForeignKey("users.user_id"), primary_key=True)
    hash: Mapped[str] = mapped_column(String(255))
