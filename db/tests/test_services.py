from sqlalchemy import create_engine
from sqlalchemy.orm import Session

from salary_app.entities import User

from ..services import SqlPermissionsHandler, SqlAuthorizationService
from ..storage import SqlUserStorage
from ..tables import Base, SqlUser


def create_engine_inmemory():
    e = create_engine("sqlite://", echo=True)
    Base.metadata.create_all(e)
    return e


def test_checks_permissions_correctly():
    engine = create_engine_inmemory()
    permissions_handler = SqlPermissionsHandler(engine)
    user_storage = SqlUserStorage(engine)

    user_admin = User(user_id="0", login="admin")
    user_1 = User(user_id="1", login="user_1")
    user_2 = User(user_id="2", login="user_2")

    assert not permissions_handler.is_admin("0")
    assert not permissions_handler.is_admin("1")
    assert not permissions_handler.is_admin("2")

    user_storage.create(user_admin)

    assert not permissions_handler.is_admin("0")
    assert not permissions_handler.is_admin("1")
    assert not permissions_handler.is_admin("2")

    permissions_handler.make_admin("0")

    assert permissions_handler.is_admin("0")
    assert not permissions_handler.is_admin("1")
    assert not permissions_handler.is_admin("2")

    user_storage.create(user_1)
    user_storage.create(user_2)

    assert permissions_handler.is_admin("0")
    assert not permissions_handler.is_admin("1")
    assert not permissions_handler.is_admin("2")


def test_auth_service_verifies_correctly():
    engine = create_engine_inmemory()
    auth_service = SqlAuthorizationService(engine)

    assert not auth_service.verify_user("0", "12345")
    assert not auth_service.verify_user("1", "0000")
    assert not auth_service.verify_user("2", "password")

    auth_service.create_user("0", "12345")

    assert auth_service.verify_user("0", "12345")
    assert not auth_service.verify_user("1", "0000")
    assert not auth_service.verify_user("2", "password")

    auth_service.create_user("1", " 0000 ")

    assert auth_service.verify_user("0", "12345")
    assert not auth_service.verify_user("1", "0000")
    assert not auth_service.verify_user("2", "password")

    auth_service.create_user("2", "PassWord")

    assert auth_service.verify_user("0", "12345")
    assert not auth_service.verify_user("1", "0000")
    assert not auth_service.verify_user("2", "password")
