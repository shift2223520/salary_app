from salary_app.salary_app import SalaryApp
from salary_app.rest_service import make_rest_service

from db.main import connect_and_init_if_needed
from db.services import SqlPermissionsHandler, SqlAuthorizationService
from db.storage import SqlStorage

import config

engine = connect_and_init_if_needed(config.db_url)

permissions_handler = SqlPermissionsHandler(engine)
auth_service = SqlAuthorizationService(engine)
storage = SqlStorage(engine)

salary_app = SalaryApp(
    storage=storage,
    auth_service=auth_service,
    permissions_handler=permissions_handler,
)

app = make_rest_service(salary_app, config.session_token_expiration_period)
