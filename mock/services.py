from typing import List
from dataclasses import dataclass

from salary_app.salary_app import AuthorizationService, PermissionsHandler
from salary_app.entities import UserId


@dataclass
class MockUserAuthRecord:
    user_id: UserId
    password: str


class MockAuthorizationService(AuthorizationService):
    def __init__(self, empty: bool = False):

        self.users: List[MockUserAuthRecord] = []
        if not empty:
            self.users = [
                MockUserAuthRecord(user_id="0", password="admin"),
                MockUserAuthRecord(user_id="1", password="12345"),
                MockUserAuthRecord(user_id="2", password="12345"),
            ]

    def verify_user(self, user_id: UserId, password: str) -> bool:
        return any((user.user_id == user_id and user.password == password for user in self.users))

    def create_user(self, user_id: UserId, password: str):
        self.users.append(
            MockUserAuthRecord(user_id=user_id, password=password)
        )


class MockPermissionsHandler(PermissionsHandler):
    def is_admin(self, user_id: UserId) -> bool:
        return user_id == "0"
