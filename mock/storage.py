import datetime
from dataclasses import dataclass
from datetime import date
from typing import Optional, List

from decimal import Decimal

from salary_app.entities import EmployeeId, UserId, Salary, User, Employee
from salary_app.storage import UserStorage, EmployeeStorage, PromotionStorage, Storage


@dataclass
class MockEmployeeRecord:
    employee_id: EmployeeId
    user_id: UserId
    name: str
    salary: Salary


@dataclass
class MockUserRecord:
    user_id: UserId
    login: str


@dataclass
class MockPromotionRecord:
    date: datetime.date
    employee_id: EmployeeId


class MockUserStorage(UserStorage):
    def __init__(self, empty: bool = False):
        self.users = []
        if not empty:
            self.users = [
                MockUserRecord(user_id="0", login="admin"),
                MockUserRecord(user_id="1", login="User1"),
                MockUserRecord(user_id="2", login="User2"),
            ]

    def fetch(self, user_id: UserId) -> Optional[User]:
        for user in self.users:
            if user.user_id == user_id:
                return User(user_id=user.user_id, login=user.login)
        return None

    def create(self, user: User):
        self.users.append(
            MockUserRecord(user_id=user.user_id, login=user.login)
        )

    def find_by_login(self, login: str) -> Optional[UserId]:
        for user in self.users:
            if user.login.lower() == login.lower():
                return user.user_id
        return None


class MockEmployeeStorage(EmployeeStorage):

    def __init__(self, empty: bool = False):
        self.employees: List[MockEmployeeRecord] = []
        if not empty:
            self.employees = [
                MockEmployeeRecord(
                    employee_id="0001",
                    user_id="1",
                    name="User1",
                    salary=Decimal(25000),
                ),

                MockEmployeeRecord(
                    employee_id="0002",
                    user_id="2",
                    name="User2",
                    salary=Decimal("300000.5"),
                ),
            ]

    def fetch(self, employee_id: EmployeeId) -> Optional[Employee]:
        for employee in self.employees:
            if employee.employee_id == employee_id:
                return Employee(
                    employee_id=employee.employee_id,
                    name=employee.name,
                    salary=employee.salary,
                )
        return None

    def create(self, user_id: UserId, employee: Employee):
        self.employees.append(MockEmployeeRecord(
            employee_id=employee.employee_id,
            user_id=user_id,
            name=employee.name,
            salary=employee.salary,
        ))

    def find_by_user_id(self, user_id: UserId) -> Optional[EmployeeId]:
        for employee in self.employees:
            if employee.user_id == user_id:
                return employee.employee_id
        return None


class MockPromotionStorage(PromotionStorage):
    def __init__(self, empty: bool = False):
        self.promotions: List[MockPromotionRecord] = []
        if not empty:
            self.promotions = [
                MockPromotionRecord(
                    employee_id="0001",
                    date=datetime.date(2025, 1, 15)
                )
            ]

    def fetch_upcoming(self, employee_id: EmployeeId) -> Optional[date]:
        promotions_filtered = filter(
            lambda record: (record.employee_id == employee_id),
            self.promotions
        )
        promotions_sorted = sorted(promotions_filtered, key=lambda record: record.date)
        if len(promotions_sorted) == 0:
            return None
        else:
            return promotions_sorted[0].date

    def create(self, employee_id: EmployeeId, promotion_date: datetime.date):
        self.promotions.append(
            MockPromotionRecord(employee_id=employee_id, date=promotion_date)
        )


class MockStorage(Storage):
    def __init__(self, empty: bool = False):
        self._user = MockUserStorage(empty)
        self._employee = MockEmployeeStorage(empty)
        self._promotion = MockPromotionStorage(empty)

    @property
    def user(self) -> UserStorage:
        return self._user

    @property
    def employee(self) -> EmployeeStorage:
        return self._employee

    @property
    def promotion(self) -> PromotionStorage:
        return self._promotion
