from decimal import Decimal
from dataclasses import dataclass

UserId = str
EmployeeId = str
Salary = Decimal


@dataclass
class Employee:
    employee_id: EmployeeId
    name: str
    salary: Salary


@dataclass
class User:
    user_id: UserId
    login: str
