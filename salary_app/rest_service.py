import datetime
import uuid
from threading import Lock
from typing import Dict, Optional, Annotated
from functools import wraps
from dataclasses import dataclass

from fastapi import FastAPI, APIRouter, Depends, HTTPException
from fastapi.responses import JSONResponse
from fastapi import status
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from pydantic import BaseModel

from salary_app.salary_app import SalaryApp, AccessDenied, EmployeeNotFound, UniqueKeyConstraintViolation
from salary_app.entities import UserId, EmployeeId, Salary

Token = str


@dataclass
class TokenInfo:
    user_id: UserId
    expiration_date: datetime.datetime

    def expired(self):
        return datetime.datetime.now() > self.expiration_date


class SessionTokenHandler:
    def __init__(self, expiration_period: datetime.timedelta):
        self._tokens: Dict[Token, TokenInfo] = {}
        self._expiration_period = expiration_period
        self._lock = Lock()

    def create_token(self, user_id: UserId) -> Token:
        # Thread-safe. Can be called from both sync and async contexts
        with self._lock:
            self._remove_expired_tokens()
            new_token = str(uuid.uuid4())
            expiration_date = datetime.datetime.now() + self._expiration_period
            self._tokens[new_token] = TokenInfo(user_id=user_id, expiration_date=expiration_date)

            return new_token

    def verify_token(self, token: Token) -> Optional[UserId]:
        # Thread-safe. Can be called from both sync and async contexts
        with self._lock:
            if token not in self._tokens:
                return None

            token_info = self._tokens[token]
            if token_info.expired():
                return None

            return token_info.user_id

    def _remove_expired_tokens(self):
        expired_tokens = [token for (token, token_info) in self._tokens.items() if token_info.expired()]
        for expired_token in expired_tokens:
            del self._tokens[expired_token]


oauth_scheme = OAuth2PasswordBearer(tokenUrl="authorize")


class InvalidToken(Exception):
    pass


class Unauthorized(Exception):
    pass


class UserModel(BaseModel):
    login: str
    password: str


class EmployeeModel(BaseModel):
    name: str
    user_id: UserId
    salary: Salary


class PromotionModel(BaseModel):
    employee_id: EmployeeId
    date: datetime.date


def make_rest_service(salary_app: SalaryApp, session_token_expiration_period: datetime.timedelta) -> FastAPI:
    app = FastAPI()
    debug_router = APIRouter(prefix="/debug", tags=["debug"])

    session_token_handler = SessionTokenHandler(session_token_expiration_period)

    def user_id_from_token(token: Annotated[Token, Depends(oauth_scheme)]) -> Optional[UserId]:
        return session_token_handler.verify_token(token)

    def result_wrapper(func):
        @wraps(func)
        async def inner(*args, **kwargs):
            response = None
            error_message = None
            status_code = status.HTTP_200_OK
            try:
                response = await func(*args, **kwargs)
            except AccessDenied:
                status_code = status.HTTP_403_FORBIDDEN
                error_message = "Access denied"
            except Unauthorized:
                status_code = status.HTTP_401_UNAUTHORIZED
                error_message = "Unauthorized"
            except EmployeeNotFound:
                status_code = status.HTTP_404_NOT_FOUND
                error_message = "Employee not found"
            except InvalidToken:
                status_code = status.HTTP_401_UNAUTHORIZED
                error_message = "Invalid token"
            except UniqueKeyConstraintViolation as e:
                status_code = status.HTTP_400_BAD_REQUEST
                error_message = f"Unique key constraint violation on field: {e}"
            except Exception as e:
                status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
                error_message = f"Server error: {repr(e)}"

            if error_message is None:
                return JSONResponse({"success": True, "result": response}, status_code=status_code)
            else:
                return JSONResponse({"success": False, "error": error_message}, status_code=status_code)

        return inner

    @app.post("/authorize")
    async def post_authorize(form_data: Annotated[OAuth2PasswordRequestForm, Depends()]):
        user_id = salary_app.verify_user(form_data.username, form_data.password)
        if user_id is None:
            raise HTTPException(status_code=400, detail="Unauthorized")

        token = session_token_handler.create_token(user_id)
        return {"access_token": token, "token_type": "bearer"}

    @app.get("/salary")
    @result_wrapper
    async def get_salary(user_id: Annotated[Optional[UserId], Depends(user_id_from_token)]):
        if user_id is None:
            raise InvalidToken()

        return str(salary_app.get_salary(user_id))

    @app.get("/promotion_date")
    @result_wrapper
    async def get_promotion_date(user_id: Annotated[Optional[UserId], Depends(user_id_from_token)]):
        if user_id is None:
            raise InvalidToken()

        promotion_date = salary_app.get_promotion_date(user_id)
        if promotion_date is None:
            return None
        else:
            return promotion_date.isoformat()

    @debug_router.put("/user")
    @result_wrapper
    async def put_user(user_id: Annotated[Optional[UserId], Depends(user_id_from_token)], user: UserModel):
        if user_id is None:
            raise InvalidToken()

        new_user_id = salary_app.create_user(
            authorized_user=user_id,
            login=user.login,
            password=user.password,
        )

        return new_user_id

    @debug_router.put("/employee")
    @result_wrapper
    async def put_employee(user_id: Annotated[Optional[UserId], Depends(user_id_from_token)], employee: EmployeeModel):
        if user_id is None:
            raise InvalidToken()

        new_employee_id = salary_app.create_employee(
            authorized_user=user_id,
            employee_name=employee.name,
            employee_user_id=employee.user_id,
            employee_salary=employee.salary,
        )

        return new_employee_id

    @debug_router.put("/promotion")
    @result_wrapper
    async def put_promotion(user_id: Annotated[Optional[UserId], Depends(user_id_from_token)], promotion: PromotionModel):
        if user_id is None:
            raise InvalidToken()

        salary_app.plan_promotion(
            authorized_user=user_id,
            employee_id=promotion.employee_id,
            promotion_date=promotion.date
        )

    app.include_router(debug_router)
    return app
