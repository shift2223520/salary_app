import datetime
from datetime import date
import uuid
from abc import ABCMeta, abstractmethod
from typing import Optional

from .storage import Storage
from .entities import UserId, User, EmployeeId, Employee, Salary


class AuthorizationService(metaclass=ABCMeta):

    @abstractmethod
    def verify_user(self, user_id: UserId, password: str) -> bool:
        """Verifies user, given login and password. If user is not verified returns None"""
        pass

    @abstractmethod
    def create_user(self, user_id: UserId, password: str):
        pass


class PermissionsHandler(metaclass=ABCMeta):
    @abstractmethod
    def is_admin(self, user_id: UserId) -> bool:
        pass


class EmployeeNotFound(Exception):
    pass


class AccessDenied(Exception):
    pass


class UniqueKeyConstraintViolation(Exception):
    pass


class SalaryApp:
    def __init__(self, auth_service: AuthorizationService, storage: Storage, permissions_handler: PermissionsHandler):
        self._auth_service = auth_service
        self._storage = storage
        self._permissions_handler = permissions_handler

    def verify_user(self, login: str, password: str) -> Optional[UserId]:
        login = login.lower()

        user_id = self._storage.user.find_by_login(login)
        if user_id is None:
            return None

        if self._auth_service.verify_user(user_id, password):
            return user_id
        else:
            return None

    def get_salary(self, authorized_user: UserId) -> Salary:
        employee_id = self._storage.employee.find_by_user_id(authorized_user)
        if employee_id is None:
            raise EmployeeNotFound()

        employee = self._storage.employee.fetch(employee_id)
        if employee is None:
            # Employee was deleted
            raise EmployeeNotFound()

        return employee.salary

    def get_promotion_date(self, authorized_user: UserId) -> Optional[date]:
        employee_id = self._storage.employee.find_by_user_id(authorized_user)
        if employee_id is None:
            raise EmployeeNotFound()

        promotion_date = self._storage.promotion.fetch_upcoming(employee_id)

        return promotion_date

    def create_employee(self, authorized_user: UserId, employee_user_id: UserId, employee_name: str, employee_salary: Salary) -> EmployeeId:
        if not self._permissions_handler.is_admin(authorized_user):
            raise AccessDenied()

        if self._storage.employee.find_by_user_id(employee_user_id) is not None:
            raise UniqueKeyConstraintViolation("user_id")

        employee_id = str(uuid.uuid4())

        if self._storage.employee.fetch(employee_id) is not None:
            raise UniqueKeyConstraintViolation("employee_id")

        self._storage.employee.create(
            user_id=employee_user_id,
            employee=Employee(
                employee_id=employee_id,
                name=employee_name,
                salary=employee_salary,
            )
        )

        return employee_id

    def create_user(self, authorized_user: UserId, login: str, password: str) -> UserId:
        if not self._permissions_handler.is_admin(authorized_user):
            raise AccessDenied()

        login = login.lower()

        if self._storage.user.find_by_login(login) is not None:
            raise UniqueKeyConstraintViolation("login")

        new_user_id = str(uuid.uuid4())

        if self._storage.user.fetch(new_user_id) is not None:
            raise UniqueKeyConstraintViolation("user_id")

        self._storage.user.create(
            User(
                user_id=new_user_id,
                login=login,
            )
        )

        self._auth_service.create_user(new_user_id, password)

        return new_user_id

    def plan_promotion(self, authorized_user: UserId, employee_id: EmployeeId, promotion_date: datetime.date):
        if not self._permissions_handler.is_admin(authorized_user):
            raise AccessDenied()

        if self._storage.employee.fetch(employee_id) is None:
            raise EmployeeNotFound()

        self._storage.promotion.create(employee_id, promotion_date)
