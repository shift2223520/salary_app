import datetime
from datetime import date
from abc import ABCMeta, abstractmethod
from typing import Optional

from .entities import User, UserId, Employee, EmployeeId


class UserStorage(metaclass=ABCMeta):
    @abstractmethod
    def fetch(self, user_id: UserId) -> Optional[User]:
        pass

    @abstractmethod
    def create(self, user: User):
        pass

    @abstractmethod
    def find_by_login(self, login: str) -> Optional[UserId]:
        pass


class EmployeeStorage(metaclass=ABCMeta):
    @abstractmethod
    def fetch(self, employee_id: EmployeeId) -> Optional[Employee]:
        pass

    @abstractmethod
    def create(self, user_id: UserId, employee: Employee):
        pass

    @abstractmethod
    def find_by_user_id(self, user_id: UserId) -> Optional[EmployeeId]:
        pass


class PromotionStorage(metaclass=ABCMeta):
    @abstractmethod
    def fetch_upcoming(self, employee_id: EmployeeId) -> Optional[date]:
        pass

    @abstractmethod
    def create(self, employee_id: EmployeeId, promotion_date: datetime.date):
        pass


class Storage(metaclass=ABCMeta):
    @property
    @abstractmethod
    def user(self) -> UserStorage:
        pass

    @property
    @abstractmethod
    def employee(self) -> EmployeeStorage:
        pass

    @property
    @abstractmethod
    def promotion(self) -> PromotionStorage:
        pass
