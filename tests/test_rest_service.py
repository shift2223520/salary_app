from datetime import timedelta

from fastapi.testclient import TestClient

from salary_app.salary_app import SalaryApp
from salary_app.rest_service import make_rest_service
from mock.services import MockAuthorizationService, MockPermissionsHandler
from mock.storage import MockStorage


def make_test_client():
    salary_app = SalaryApp(
        auth_service=MockAuthorizationService(),
        storage=MockStorage(),
        permissions_handler=MockPermissionsHandler(),
    )
    rest_service = make_rest_service(salary_app, timedelta(hours=1))
    return TestClient(rest_service)


def authorization_response(test_client: TestClient, login: str, password: str):
    return test_client.post(
        url="/authorize",
        content=f"grant_type=&username={login}&password={password}&scope=&client_id=&client_secret=",
        headers={
            "accept": "application / json",
            "Content-Type": "application/x-www-form-urlencoded",
        }
    )


def auth_header(test_client: TestClient, login: str, password: str) -> dict:
    response = authorization_response(test_client, login, password)
    access_token = response.json()["access_token"]
    return {"Authorization": f"Bearer {access_token}"}


def test_rest_service_handles_invalid_tokens():
    test_client = make_test_client()

    methods = [
        ("get", "/salary", None),
        ("get", "/promotion_date", None),
        ("put", "/debug/user", '{"login": "__new_user", "password": "12345"}'),
        ("put", "/debug/employee", '{"name": "", "user_id": "", "salary": 0}'),
        ("put", "/debug/promotion", '{"employee_id": "", "date": "2025-01-01"}'),
    ]

    headers_wrong_token = {"Authorization": "Bearer __invalid_token"}

    for (action, url, content) in methods:
        if action == "get":
            response = test_client.get(url, headers=headers_wrong_token)
        elif action == "put":
            response = test_client.put(url, headers=headers_wrong_token, content=content)
        else:
            raise Exception(f"Unknown HTTP verb: {action}")

        assert response.status_code == 401
        assert not response.json()["success"]


def test_rest_service_handles_authorization():
    test_client = make_test_client()
    response = authorization_response(test_client, "admin", "admin")

    assert response.status_code == 200
    assert "access_token" in response.json()


def test_mock_user_1():
    test_client = make_test_client()
    header = auth_header(test_client, "User1", "12345")

    salary_response = test_client.get("/salary", headers=header)
    assert salary_response.status_code == 200
    assert salary_response.json()["success"]
    assert salary_response.json()["result"] == "25000"

    promotion_response = test_client.get("/promotion_date", headers=header)
    assert promotion_response.status_code == 200
    assert promotion_response.json()["success"]
    assert promotion_response.json()["result"] == "2025-01-15"


def test_mock_user_2():
    test_client = make_test_client()
    header = auth_header(test_client, "User2", "12345")

    salary_response = test_client.get("/salary", headers=header)
    assert salary_response.status_code == 200
    assert salary_response.json()["success"]
    assert salary_response.json()["result"] == "300000.5"

    promotion_response = test_client.get("/promotion_date", headers=header)
    assert promotion_response.status_code == 200
    assert promotion_response.json()["success"]
    assert promotion_response.json()["result"] is None
