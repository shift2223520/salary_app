import datetime
from decimal import Decimal
import pytest

from salary_app.salary_app import SalaryApp, EmployeeNotFound
from mock.services import MockPermissionsHandler, MockAuthorizationService
from mock.storage import MockStorage


def test_provides_correct_salary():
    app = make_mock_app_instance()

    assert app.get_salary("1") == Decimal(25000)
    assert app.get_salary("2") == Decimal("300000.5")

    with pytest.raises(EmployeeNotFound):
        # admin
        app.get_salary("0")


def test_provides_correct_promotion_date():
    app = make_mock_app_instance()

    assert app.get_promotion_date("1") == datetime.date(2025, 1, 15)
    assert app.get_promotion_date("2") is None

    with pytest.raises(EmployeeNotFound):
        # admin
        app.get_salary("0")


def test_ignores_case_for_login():
    app = make_mock_app_instance()

    admin_user_id = "0"

    app.create_user(authorized_user=admin_user_id, login="NewUser3", password="12345")
    assert app.verify_user("NewUser3", "12345") is not None
    assert app.verify_user("newuser3", "12345") is not None
    assert app.verify_user("NeWuSer3", "12345") is not None
    assert app.verify_user("NewUser3", "__wrong_password") is None
    assert app.verify_user("newuser3", "__wrong_password") is None


def make_mock_app_instance():
    permissions_handler = MockPermissionsHandler()
    auth_service = MockAuthorizationService()
    storage = MockStorage()
    app = SalaryApp(
        storage=storage,
        auth_service=auth_service,
        permissions_handler=permissions_handler,
    )
    return app
