import datetime
import unittest.mock

from salary_app.rest_service import SessionTokenHandler


def test_session_token_handler_works():
    session_token_handler = SessionTokenHandler(datetime.timedelta(hours=1))

    assert session_token_handler.verify_token("__invalid_token") is None

    new_token = session_token_handler.create_token("__test_user_id")
    assert session_token_handler.verify_token(new_token) == "__test_user_id"


def test_session_token_expires():
    session_token_handler = SessionTokenHandler(datetime.timedelta(hours=1))

    new_token = session_token_handler.create_token("__test_user_id")
    assert session_token_handler.verify_token(new_token) == "__test_user_id"

    time_forward(datetime.timedelta(hours=1, microseconds=1))
    assert session_token_handler.verify_token(new_token) is None


def time_forward(timedelta: datetime.timedelta):
    new_current_time = datetime.datetime.now() + timedelta
    datetime.datetime = unittest.mock.MagicMock()
    datetime.datetime.now.return_value = new_current_time
