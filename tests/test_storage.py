from datetime import date
from decimal import Decimal
import pytest

from sqlalchemy import create_engine

from salary_app.entities import User, Employee
from salary_app.storage import UserStorage, EmployeeStorage, PromotionStorage, Storage

from db.tables import Base
from db.storage import SqlStorage

from mock.storage import MockStorage


def create_engine_inmemory():
    e = create_engine("sqlite://", echo=True)
    Base.metadata.create_all(e)
    return e


@pytest.fixture(params=["sql", "mock"])
def storage(request) -> Storage:
    if request.param == "sql":
        engine = create_engine_inmemory()
        return SqlStorage(engine)
    elif request.param == "mock":
        return MockStorage(empty=True)
    else:
        raise Exception(f"unknown fixture param: {request.param}")


@pytest.fixture
def user_storage(storage):
    return storage.user


@pytest.fixture
def employee_storage(storage):
    return storage.employee


@pytest.fixture
def promotion_storage(storage):
    return storage.promotion


def test_creates_user(user_storage):
    user_storage.create(User(user_id="1", login="user_1"))


def test_fetches_user_correctly(user_storage: UserStorage):

    user_1 = User(user_id="1", login="user_1")
    user_2 = User(user_id="2", login="user_2")

    assert user_storage.fetch("__invalid_id") is None
    assert user_storage.fetch("1") is None
    assert user_storage.fetch("2") is None

    user_storage.create(user_1)

    assert user_storage.fetch("__invalid_id") is None
    assert user_storage.fetch("1") == user_1
    assert user_storage.fetch("2") is None

    user_storage.create(user_2)

    assert user_storage.fetch("__invalid_id") is None
    assert user_storage.fetch("1") == user_1
    assert user_storage.fetch("2") == user_2


def test_finds_user_by_login_correctly(user_storage: UserStorage):

    user_1 = User(user_id="1", login="user_1")
    user_2 = User(user_id="2", login="user_2")

    assert user_storage.find_by_login("__invalid_login") is None
    assert user_storage.find_by_login("user_1") is None
    assert user_storage.find_by_login("user_2") is None

    user_storage.create(user_1)

    assert user_storage.find_by_login("__invalid_login") is None
    assert user_storage.find_by_login("user_1") == user_1.user_id
    assert user_storage.find_by_login("user_2") is None

    user_storage.create(user_2)

    assert user_storage.find_by_login("__invalid_login") is None
    assert user_storage.find_by_login("user_1") == user_1.user_id
    assert user_storage.find_by_login("user_2") == user_2.user_id


def test_creates_employee(user_storage: UserStorage, employee_storage: EmployeeStorage):
    user_storage.create(User(user_id="1", login="user_1"))
    employee_storage.create("1", Employee(employee_id="1", name="User 1", salary=Decimal(15000)))


def test_fetches_employee_correctly(employee_storage: EmployeeStorage):

    user_1 = User(user_id="1", login="user_1")
    employee_1 = Employee(employee_id="1", name="User 1", salary=Decimal(25000))

    user_2 = User(user_id="2", login="user_2")
    employee_2 = Employee(employee_id="2", name="User 2", salary=Decimal("15000.50"))

    assert employee_storage.fetch("__invalid_id") is None
    assert employee_storage.fetch("1") is None
    assert employee_storage.fetch("2") is None

    employee_storage.create(user_1.user_id, employee_1)

    assert employee_storage.fetch("__invalid_id") is None
    assert employee_storage.fetch("1") == employee_1
    assert employee_storage.fetch("2") is None

    employee_storage.create(user_2.user_id, employee_2)

    assert employee_storage.fetch("__invalid_id") is None
    assert employee_storage.fetch("1") == employee_1
    assert employee_storage.fetch("2") == employee_2


def test_finds_employee_by_user_id_correctly(employee_storage: EmployeeStorage):

    user_1 = User(user_id="0001", login="user_1")
    employee_1 = Employee(employee_id="1", name="User 1", salary=Decimal(25000))

    user_2 = User(user_id="0002", login="user_2")
    employee_2 = Employee(employee_id="2", name="User 2", salary=Decimal("15000.50"))

    assert employee_storage.find_by_user_id("__invalid_id") is None
    assert employee_storage.find_by_user_id(user_1.user_id) is None
    assert employee_storage.find_by_user_id(user_2.user_id) is None

    employee_storage.create(user_1.user_id, employee_1)

    assert employee_storage.find_by_user_id("__invalid_id") is None
    assert employee_storage.find_by_user_id(user_1.user_id) == employee_1.employee_id
    assert employee_storage.find_by_user_id(user_2.user_id) is None

    employee_storage.create(user_2.user_id, employee_2)

    assert employee_storage.find_by_user_id("__invalid_id") is None
    assert employee_storage.find_by_user_id(user_1.user_id) == employee_1.employee_id
    assert employee_storage.find_by_user_id(user_2.user_id) == employee_2.employee_id


def test_promotion_date(promotion_storage: PromotionStorage, employee_storage: EmployeeStorage, user_storage: UserStorage):
    user_1 = User(user_id="1", login="user_1")
    employee_1 = Employee(employee_id="0001", name="User 1", salary=Decimal(36000))

    user_2 = User(user_id="2", login="user_2")
    employee_2 = Employee(employee_id="0002", name="User 2", salary=Decimal(10000))

    user_storage.create(user_1)
    employee_storage.create(employee=employee_1, user_id=user_1.user_id)

    user_storage.create(user_2)
    employee_storage.create(employee=employee_2, user_id=user_2.user_id)

    assert promotion_storage.fetch_upcoming(employee_1.employee_id) is None
    assert promotion_storage.fetch_upcoming(employee_2.employee_id) is None

    promotion_storage.create(employee_id=employee_1.employee_id, promotion_date=date(2024, 1, 15))
    assert promotion_storage.fetch_upcoming(employee_1.employee_id) == date(2024, 1, 15)
    assert promotion_storage.fetch_upcoming(employee_2.employee_id) is None

    promotion_storage.create(employee_id=employee_1.employee_id, promotion_date=date(2024, 2, 15))
    assert promotion_storage.fetch_upcoming(employee_1.employee_id) == date(2024, 1, 15)
    assert promotion_storage.fetch_upcoming(employee_2.employee_id) is None

    promotion_storage.create(employee_id=employee_1.employee_id, promotion_date=date(2025, 1, 1))
    assert promotion_storage.fetch_upcoming(employee_1.employee_id) == date(2024, 1, 15)
    assert promotion_storage.fetch_upcoming(employee_2.employee_id) is None

    promotion_storage.create(employee_id=employee_1.employee_id, promotion_date=date(2023, 5, 10))
    assert promotion_storage.fetch_upcoming(employee_1.employee_id) == date(2023, 5, 10)
    assert promotion_storage.fetch_upcoming(employee_2.employee_id) is None

    promotion_storage.create(employee_id=employee_2.employee_id, promotion_date=date(2026, 1, 1))
    assert promotion_storage.fetch_upcoming(employee_1.employee_id) == date(2023, 5, 10)
    assert promotion_storage.fetch_upcoming(employee_2.employee_id) == date(2026, 1, 1)

